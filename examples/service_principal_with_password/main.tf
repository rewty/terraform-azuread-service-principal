# Azurerm provider configuration
provider "azurerm" {
  features {}
}

module "service-principal" {
  source  = "./modules/terraform-azurerm-service-principal"

  service_principal_name     = "simple-appaccess"
  password_rotation_in_years = 1

  # Adding roles and scope to service principal
  assignments = [
    {
      scope                = "/subscriptions/xxxxx000-0000-0000-0000-xxxx0000xxxx"
      role_definition_name = "Contributor"
    },
  ]
}
